# system modules
import os
import shutil
import unittest
from unittest.mock import patch

# internal modules
from sensemapi.account import SenseMapAccount
from sensemapi.cache import SQLiteAuthentiCache
from sensemapi.xdg import *

from . import API_SERVER, needs_credentials

# external modules

CACHE_DIR = os.path.abspath(".test-cache")


class SenseMapCacheTest(unittest.TestCase):
    def setUp(self):
        self.assertFalse(
            os.path.exists(CACHE_DIR),
            "the cache directory '{}' exists before the test".format(
                CACHE_DIR
            ),
        )

    def tearDown(self):
        if os.path.exists(CACHE_DIR):
            shutil.rmtree(CACHE_DIR)


class SenseMapSQLiteCacheTest(SenseMapCacheTest):
    @patch.dict("os.environ", {"XDG_CACHE_HOME": CACHE_DIR})
    def test_directory_defaults_to_xdg_cache_home_package_dir(self):
        self.assertEqual(
            SQLiteAuthentiCache().directory,
            XDGPackageDirectory("XDG_CACHE_HOME").path,
        )


class SenseMapAccountCacheTest(SenseMapCacheTest):
    def test_cache_delete_deletes_whole_cache(self):
        account1 = SenseMapAccount(
            email="random@email.com",
            auth_cache=SQLiteAuthentiCache(directory=CACHE_DIR),
            api=API_SERVER,
        )
        account2 = SenseMapAccount(
            email="anotherrandom@email.com",
            auth_cache=SQLiteAuthentiCache(directory=CACHE_DIR),
            api=API_SERVER,
        )
        self.assertGreater(len(account1.auth_cache.get()), 0)
        account1.auth_cache.delete()
        self.assertEqual(len(account1.auth_cache.get()), 0)

    @needs_credentials
    def test_second_account_without_password_can_use_cached_tokens(self):
        account1 = SenseMapAccount(
            email=os.environ.get("SENSEMAP_EMAIL"),
            username=os.environ.get("SENSEMAP_USER"),
            password=os.environ.get("SENSEMAP_PASSWORD"),
            auth_cache=SQLiteAuthentiCache(directory=CACHE_DIR),
            api=API_SERVER,
        )
        account1.sign_in()
        account2 = SenseMapAccount(
            email=os.environ.get("SENSEMAP_EMAIL"),
            username=os.environ.get("SENSEMAP_USER"),
            auth_cache=SQLiteAuthentiCache(directory=CACHE_DIR),
            api=API_SERVER,
        )
        account2.get_own_boxes()

    @needs_credentials
    def test_cache_password_interactivity(self):
        account = SenseMapAccount(
            email=os.environ.get("SENSEMAP_EMAIL"),
            username=os.environ.get("SENSEMAP_USER"),
            password=os.environ.get("SENSEMAP_PASSWORD"),
            auth_cache=SQLiteAuthentiCache(directory=CACHE_DIR),
            cache_password=False,
            api=API_SERVER,
        )
        self.assertFalse(
            any(map(lambda x: x.get("password"), account.auth_cache.get())),
            "password was cached despite settings",
        )
        account.sign_in()
        self.assertIsNotNone(account.password)
        self.assertFalse(
            any(map(lambda x: x.get("password"), account.auth_cache.get())),
            "password was cached despite settings",
        )
        account.cache_password = True
        self.assertTrue(
            any(map(lambda x: x.get("password"), account.auth_cache.get())),
            "password was not cached",
        )
        account.cache_password = False
        self.assertFalse(
            any(map(lambda x: x.get("password"), account.auth_cache.get())),
            "password was cached despite settings",
        )

    def test_two_separate_accounts_see_same_cache(self):
        account1 = SenseMapAccount(
            email="random@email.com",
            auth_cache=SQLiteAuthentiCache(directory=CACHE_DIR),
            api=API_SERVER,
        )
        account2 = SenseMapAccount(
            email="another-random@email.com",
            auth_cache=SQLiteAuthentiCache(directory=CACHE_DIR),
            api=API_SERVER,
        )
        self.assertSequenceEqual(
            account1.auth_cache.get(),
            account2.auth_cache.get(),
            "two separate accounts don't see the same cache",
        )
        self.assertEqual(
            len(account1.auth_cache.get()),
            2,
            "number of cache entries does not equal number of accounts",
        )

    @needs_credentials
    def test_same_accounts_on_different_apis_dont_interfere(self):
        account1 = SenseMapAccount(
            email=os.environ.get("SENSEMAP_EMAIL"),
            username=os.environ.get("SENSEMAP_USER"),
            password=os.environ.get("SENSEMAP_PASSWORD"),
            auth_cache=SQLiteAuthentiCache(directory=CACHE_DIR),
            cache_password=True,
            api=API_SERVER,
        )
        self.assertEqual(len(account1.auth_cache.get()), 1)
        account2 = SenseMapAccount(
            email=account1.email,
            username=account1.username,
            password=account1.password,
            auth_cache=account1.auth_cache,
            cache_password=account1.cache_password,
            api=account1.api + ".anotherapi",
        )
        self.assertEqual(len(account1.auth_cache.get()), 2)
        account1.sign_in()
        account1_cache = account1.auth_cache.get(account1.authenticache)
        account2_cache = account2.auth_cache.get(account2.authenticache)
        self.assertEqual(
            len(account1_cache),
            1,
            "first account has not exactly one cache match but {}".format(
                len(account1_cache)
            ),
        )
        self.assertEqual(
            len(account2_cache),
            1,
            "second account has not exactly one cache match but {}".format(
                len(account2_cache)
            ),
        )
        account1_cache = account1_cache[0]
        account2_cache = account2_cache[0]
        for k, v in account1_cache.items():
            self.assertIsNotNone(
                v, "cached {} of first account is empty".format(k)
            )
        self.assertNotEqual(
            account1_cache["api"],
            account2_cache["api"],
            "cache of two equal accounts on different apis interfere",
        )

    def test_delete_cache(self):
        account1 = SenseMapAccount(
            email="random@email.com",
            auth_cache=SQLiteAuthentiCache(directory=CACHE_DIR),
            api=API_SERVER,
        )
        account2 = SenseMapAccount(
            email="another-random@email.com",
            auth_cache=SQLiteAuthentiCache(directory=CACHE_DIR),
            api=API_SERVER,
        )
        old_len = len(account1.auth_cache.get())
        account1.delete_cache()
        self.assertLess(
            len(account1.auth_cache.get()),
            old_len,
            "delete_cache did not decrease number of cache entries",
        )
        self.assertFalse(
            any(
                map(
                    lambda x: x.get("email") == "random@email.com",
                    account1.auth_cache.get(),
                )
            ),
            "delete_cache did not remove the cache entry",
        )

    def test_changing_single_values_updates_cache_entry(self):
        account = SenseMapAccount(
            email="random@email.com",
            username="user",
            auth_cache=SQLiteAuthentiCache(directory=CACHE_DIR),
            api=API_SERVER,
        )
        account.email = "another@email.com"
        self.assertEqual(
            len(account.auth_cache.get()),
            1,
            "changing single property altered the number of cache entries",
        )
        self.assertTrue(
            any(
                map(
                    lambda x: x.get("email") == "another@email.com",
                    account.auth_cache.get(),
                )
            ),
            "changing single property did not update the cache entry",
        )

    def test_changing_multiple_values_with_pause_caching_adds_new(self):
        account = SenseMapAccount(
            email="random@email.com",
            auth_cache=SQLiteAuthentiCache(directory=CACHE_DIR),
            api=API_SERVER,
        )
        with account.pause_caching():
            account.email = "another@email.com"
            account.username = "user"
        self.assertEqual(
            len(account.auth_cache.get()),
            2,
            "changing multiple values with pause_caching did not add entry",
        )
        self.assertTrue(
            any(
                map(
                    lambda x: (
                        x.get("email") == "another@email.com"
                        and x.get("username") == "user"
                    ),
                    account.auth_cache.get(),
                )
            ),
            "changing single property did not update the cache entry",
        )
