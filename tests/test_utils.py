# system modules
import datetime
import io
import unittest
from unittest.mock import Mock, patch

from sensemapi.utils import *

# external modules


class SenseMapUtilsTest(unittest.TestCase):
    def test_catch_returns_on_specified_exception(self):
        self.assertEqual(
            catch({ZeroDivisionError: 100}, lambda x: x / 0, 1), 100
        )

    def test_catch_raises_other_exception(self):
        with self.assertRaises(ZeroDivisionError):
            catch({ValueError: 100}, lambda x: x / 0, 1)

    def test_catch_returns_normally_without_exception(self):
        self.assertEqual(catch({ValueError: 100}, lambda: 1), 1)


class SenseMapDateUtilsTest(unittest.TestCase):
    dates = {
        "2016-06-02T11:22:51.817Z": datetime.datetime(
            2016, 6, 2, 11, 22, 51, 817000, tzinfo=datetime.timezone.utc
        ),
        "2016-06-02T11:22:51.000Z": datetime.datetime(
            2016, 6, 2, 11, 22, 51, 0, tzinfo=datetime.timezone.utc
        ),
    }

    def test_str2date(self):
        for s, d in self.dates.items():
            self.assertLess((str2date(s) - d).total_seconds(), 1)

    def test_date2str(self):
        for s, d in self.dates.items():
            self.assertEqual(date2str(d), s)

    def test_str2date_date2str_consistency(self):
        for s, d in self.dates.items():
            self.assertLess((str2date(date2str(d)) - d).total_seconds(), 1)

    def test_date2str_str2date_consistency(self):
        for s, d in self.dates.items():
            self.assertEqual(date2str(str2date(s)), s)


class SenseMapUtilsCSVTest(unittest.TestCase):
    def setUp(self):
        self.f = io.StringIO()
        self.d = {"a": [1, 4, 7.7], "b": [2, 5, 8.8], "c": [3, 6, 9.9]}
        self.L = [
            {"a": 1, "b": 2, "c": 3},
            {"a": 4, "b": 5, "c": 6},
            {"a": 7.7, "b": 8.8, "c": 9.9},
        ]
        self.s = "a,b,c\r\n1,2,3\r\n4,5,6\r\n7.7,8.8,9.9\r\n"

    def test_read_csv(self):
        self.f = io.StringIO(self.s)
        self.assertDictEqual(read_csv(self.f), self.d)

    def test_write_csv(self):
        write_csv(self.f, self.d)
        self.assertEqual(self.f.getvalue(), self.s)

    def test_listdict_to_dictlist(self):
        self.assertSequenceEqual(list(listdict_to_dictlist(self.d)), self.L)

    def test_dictlist_to_listdict(self):
        self.assertDictEqual(dictlist_to_listdict(self.L), self.d)

    def test_dictlist_to_listdict_listdict_to_dictlist_consistency(self):
        self.assertSequenceEqual(
            list(listdict_to_dictlist(dictlist_to_listdict(self.L))), self.L
        )

    def test_listdict_to_dictlist_dictlist_to_listdict_consistency(self):
        self.assertDictEqual(
            dictlist_to_listdict(listdict_to_dictlist(self.d)), self.d
        )
