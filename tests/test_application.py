# system modules
import os
import shutil
import unittest
import warnings
from unittest.mock import patch

from sensemapi import paths
from sensemapi.account import SenseMapAccount

# internal modules
from sensemapi.application import SenseMapApplication
from sensemapi.cache import *
from sensemapi.errors import *

from . import API_SERVER, needs_credentials

# external modules

CACHE_DIR = os.path.abspath(".test-cache")
CONFIG_DIR = os.path.abspath(".test-config")


class SenseMapApplicationBaseTest(unittest.TestCase):
    def setUp(self):
        if os.path.exists(CACHE_DIR):
            warnings.warn(
                "the cache directory '{}' existed before the test".format(
                    CACHE_DIR
                )
            )
            shutil.rmtree(CACHE_DIR)
        if os.path.exists(CONFIG_DIR):
            warnings.warn(
                "the config directory '{}' existed before the test".format(
                    CONFIG_DIR
                )
            )
            shutil.rmtree(CONFIG_DIR)

    def tearDown(self):
        if os.path.exists(CACHE_DIR):
            shutil.rmtree(CACHE_DIR)
        if os.path.exists(CONFIG_DIR):
            shutil.rmtree(CONFIG_DIR)


class SenseMapApplicationTest(SenseMapApplicationBaseTest):
    @patch.dict(
        "os.environ",
        {"XDG_CACHE_HOME": CACHE_DIR, "XDG_CONFIG_HOME": CONFIG_DIR},
    )
    def test_read_write_read_config_to_json_consistency(self):
        app = SenseMapApplication(name=type(self).__name__)
        app.accounts.append(SenseMapAccount(email="test@email.com"))
        app.accounts.append(SenseMapAccount(username="user"))
        app.accounts.append(SenseMapAccount(api=API_SERVER))
        before = app.to_json().copy()
        app.write_config()
        app.read_config()
        self.assertTrue(os.path.exists(app.config_file))
        self.assertDictEqual(app.to_json(), before)
        self.assertEqual(len(app.accounts), 3)

    @patch.dict(
        "os.environ",
        {"XDG_CACHE_HOME": CACHE_DIR, "XDG_CONFIG_HOME": CONFIG_DIR},
    )
    def test_saving_config_works(self):
        app = SenseMapApplication(name=type(self).__name__)
        app.accounts.append(SenseMapAccount(email="test@email.com"))
        app.accounts.append(SenseMapAccount(username="user"))
        app.accounts.append(SenseMapAccount(api=API_SERVER))
        before = app.to_json().copy()
        app.write_config()
        del app
        app = SenseMapApplication(name=type(self).__name__)
        self.assertTrue(os.path.exists(app.config_file))
        self.assertDictEqual(app.to_json(), before)
        self.assertEqual(len(app.accounts), 3)
