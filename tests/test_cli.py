# system modules
import unittest
import io
import os
import tempfile

# internal modules
import sensemapi.cli.commands.main as main

# external modules
from click.testing import CliRunner
import pandas as pd


class CliTest(unittest.TestCase):
    def setUp(self):
        self.runner = CliRunner()


class CallTest(CliTest):
    def test_help(self):
        self.runner.invoke(main.cli)

    def test_download(self):
        try:
            _, tmpfile = tempfile.mkstemp(suffix=".csv")
            for from_time, to_time in (
                ("2022-02-01T00:00:00+0000", "2022-03-01T00:00:00+0000"),
            ):
                result = self.runner.invoke(
                    main.cli,
                    [
                        "download",
                        "--sensebox",
                        "5d35d315953683001a2b877a",
                        "--from",
                        from_time,
                        "--to",
                        to_time,
                        "-o",
                        tmpfile,
                    ],
                )
                df = pd.read_csv(tmpfile, index_col=0, parse_dates=[0])
                self.assertGreater(len(df.index), 0, msg="no data saved")
                self.assertGreater(
                    len(df.columns), 0, msg="data has no columns"
                )
                self.assertTrue(
                    df.index.to_series().between(from_time, to_time).all(),
                    msg="data contains times outside --from --to",
                )
        finally:
            try:
                os.remove(tempfile)
            except Exception as e:
                pass
