
Installation
============

:mod:`sensemapi` is best installed via ``pip``

.. code-block:: sh

    python3 -m pip install --user sensemapi

This will install :mod:`sensemapi` from `PyPi - the Python Package Index`_.

Depending on your setup it might be necessary to install the :mod:`pip` module
first:

.. code-block:: sh

    # Debian/Ubuntu
    sudo apt-get install python3-pip

Or see `Installing PIP`_.

.. _Installing PIP: https://pip.pypa.io/en/stable/installing/
.. _PyPI - the Python Package Index: https://pypi.org/project/polt/


Extra Features
++++++++++++++

If you install :mod:`sensemapi` just like the above, only the OpenSenseMap
interface will be available. However, :mod:`sensemapi` can do more. To enable
all features, install :mod:`sensemapi` like this:

.. code-block:: sh

   python3 -m pip install 'sensemapi[mqtt,cache,pandas]'
